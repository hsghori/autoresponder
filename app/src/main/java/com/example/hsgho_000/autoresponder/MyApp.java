package com.example.hsgho_000.autoresponder;
import android.app.Application;
import android.content.Context;

class MyApp extends Application {

    private static Context mContext;

    @Override
    public void onCreate(){
        super.onCreate();
        mContext = this.getApplicationContext();
    }

    public static Context getAppContext(){
        return mContext;
    }
}

