package com.example.hsgho_000.autoresponder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by haroon on 4/27/17.
 */

public class ContactListAdapter extends ArrayAdapter<Contact> {

    private ArrayList<Contact> dataSet;
    Context mContext;
    private static final String TAG = "MA_LOG";

    private static class ViewHolder {
        TextView nameView;
        TextView numberView;
        TextView propertiesView;
    }

    public ContactListAdapter(ArrayList<Contact> data, Context context) {
        super(context, R.layout.contacts_row, data);
        this.dataSet = data;
        this.mContext=context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Contact s = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.contacts_row, parent, false);
            viewHolder.nameView = (TextView) convertView.findViewById(R.id.contact_name);
            viewHolder.numberView = (TextView) convertView.findViewById(R.id.contact_number);
            viewHolder.propertiesView = (TextView) convertView.findViewById(R.id.properties);
            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }
        viewHolder.nameView.setText(s.getName());
        viewHolder.numberView.setText(s.getNumber());
        if (s.getBlacklist() && s.getEmergency()) {
            viewHolder.propertiesView.setText("Blacklist, Emergency Contact");
        }
        else if (s.getBlacklist()) {
            viewHolder.propertiesView.setText("Blacklist");
        }
        else if (s.getEmergency()) {
            viewHolder.propertiesView.setText("Emergency Contact");
        }
        else {
            viewHolder.propertiesView.setText("");
        }
        // Return the completed view to render on screen
        return convertView;
    }

}
