package com.example.hsgho_000.autoresponder;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import java.util.ArrayList;

/**
 * Listens for incoming SMS and responds if AutoRespond is on.
 */

public class SmsReceiver extends BroadcastReceiver {

    SmsManager smsManager = SmsManager.getDefault();
    SharedPreferences pref;
    String message;
    String MESSAGE_KEY = "messageKey";
    String STATE_KEY = "stateKey";
    final String BLACKLIST_KEY = "blacklistKey";
    final String LOG_TAG = "LOG_TAG";
    static final String ACTION = "android.provider.Telephony.SMS_RECEIVED";
    static final String DEFAULT_MESSAGE = "I'm busy. Will text you back soon";
    private static final String TELEPHON_NUMBER_FIELD_NAME = "address";
    private static final String MESSAGE_BODY_FIELD_NAME = "body";
    private static final Uri SENT_MSGS_CONTET_PROVIDER = Uri.parse("content://sms/sent");

    @Override
    public void onReceive(Context context, Intent intent) {
        pref = context.getSharedPreferences("prefs", Context.MODE_PRIVATE);
        boolean state = pref.getBoolean(STATE_KEY, false);
        message = pref.getString(MESSAGE_KEY, DEFAULT_MESSAGE);
        if (state && intent.getAction().equals(ACTION)) {
            Log.i(LOG_TAG, "received SMS");
            Bundle bundle = intent.getExtras();
            final Object[] pdusObj = (Object[]) bundle.get("pdus");
            String phoneNumber = "";
            for (Object object : pdusObj) {
                SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) object);
                phoneNumber = currentMessage.getDisplayOriginatingAddress();
                Log.i(LOG_TAG, "senderNum: " + phoneNumber);
            }
            if (isOnBlacklist(phoneNumber)) {
                return;
            }
            Log.i(LOG_TAG, message);
            smsManager.sendTextMessage(phoneNumber, null, message, null, null);
            String telNumber = intent.getStringExtra("telNumber");
            String messageBody = intent.getStringExtra("messageBody");
            if (telNumber != null && messageBody != null) {
                addMessageToSent(telNumber, messageBody);
            }
        }
    }

    private void addMessageToSent(String telNumber, String messageBody) {
        ContentValues sentSms = new ContentValues();
        sentSms.put(TELEPHON_NUMBER_FIELD_NAME, telNumber);
        sentSms.put(MESSAGE_BODY_FIELD_NAME, messageBody);

        ContentResolver contentResolver = MainActivity.getContext().getContentResolver();
        contentResolver.insert(SENT_MSGS_CONTET_PROVIDER, sentSms);
    }

    private boolean isOnBlacklist(String phoneNumber) {
        ContactsDBHelper mDbHelper = new ContactsDBHelper(MainActivity.getContext());
        ArrayList<String> blacklist = new ArrayList<>();
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                ContactsDBContract.ContractsDBEntry.COLUMN_NUMBER,
        };
        // Filter results WHERE "title" = 'My Title'
        String selection = ContactsDBContract.ContractsDBEntry.COLUMN_BLACKLIST + " = ?";
        String[] selectionArgs = { "True" };

        Cursor cursor = db.query(
                ContactsDBContract.ContractsDBEntry.TABLE_NAME,
                projection, selection, selectionArgs, null, null, null
        );
        // convert each row in database to string
        while(cursor.moveToNext()) {
            String num = cursor.getString(cursor.getColumnIndexOrThrow(ContactsDBContract.ContractsDBEntry.COLUMN_NUMBER));
            blacklist.add(num);
        }
        cursor.close();
        return (blacklist.contains(phoneNumber));
    }
}
