package com.example.hsgho_000.autoresponder;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;


public class SettingsFragment extends PreferenceFragment {

    private static final String PREF_NAME = "prefs";
    private SharedPreferences pref;
    String MESSAGE_KEY = "messageKey";
    String COUNTRY_KEY = "countryKey";
    String DEFAULT_RING_KEY="defaultRingKey";
    String ACTIVE_RING_KEY="activeRingKey";

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        pref = getActivity().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);

        ((EditTextPreference) findPreference("m_key")).setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                SharedPreferences.Editor edit = pref.edit();
                edit.putString(MESSAGE_KEY, (String) newValue);
                edit.apply();
                return true;
            }
        });
        ((ListPreference) findPreference("c_key")).setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                SharedPreferences.Editor edit = pref.edit();
                edit.putString(COUNTRY_KEY, (String) newValue);
                edit.apply();
                return true;
            }
        });
        ((ListPreference) findPreference("dp_key")).setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                SharedPreferences.Editor edit = pref.edit();
                edit.putString(DEFAULT_RING_KEY, (String) newValue);
                edit.apply();
                return true;
            }
        });
        ((ListPreference) findPreference("ar_key")).setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                SharedPreferences.Editor edit = pref.edit();
                edit.putString(ACTIVE_RING_KEY, (String) newValue);
                edit.apply();
                return true;
            }
        });
    }
}
