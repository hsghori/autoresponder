package com.example.hsgho_000.autoresponder;

import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.IBinder;
import android.telephony.SmsManager;

/**
 * Created by haroon on 4/28/17.
 */

public class SentSMSLogger extends Service {

    private static final String TELEPHON_NUMBER_FIELD_NAME = "address";
    private static final String MESSAGE_BODY_FIELD_NAME = "body";
    private static final Uri SENT_MSGS_CONTET_PROVIDER = Uri.parse("content://sms/sent");
    SmsManager smsManager = SmsManager.getDefault();
    SharedPreferences pref;
    String MESSAGE_KEY = "messageKey";
    String STATE_KEY = "stateKey";
    final String BLACKLIST_KEY = "blacklistKey";
    final String LOG_TAG = "LOG_TAG";
    static final String ACTION = "android.provider.Telephony.SMS_SENT";

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        boolean state = pref.getBoolean(STATE_KEY, false);
        if (state && intent.getAction().equals(ACTION)) {
            addMessageToSentIfPossible(intent);
            stopSelf();
        }
        return Service.START_NOT_STICKY;
    }

    private void addMessageToSentIfPossible(Intent intent) {
        if (intent != null) {
            String telNumber = intent.getStringExtra("telNumber");
            String messageBody = intent.getStringExtra("messageBody");
            if (telNumber != null && messageBody != null) {
                addMessageToSent(telNumber, messageBody);
            }
        }
    }

    private void addMessageToSent(String telNumber, String messageBody) {
        ContentValues sentSms = new ContentValues();
        sentSms.put(TELEPHON_NUMBER_FIELD_NAME, telNumber);
        sentSms.put(MESSAGE_BODY_FIELD_NAME, messageBody);

        ContentResolver contentResolver = getContentResolver();
        contentResolver.insert(SENT_MSGS_CONTET_PROVIDER, sentSms);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
