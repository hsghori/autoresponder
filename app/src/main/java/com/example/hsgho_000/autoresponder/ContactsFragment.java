package com.example.hsgho_000.autoresponder;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ContactsFragment extends Fragment {

    private View view;
    private SharedPreferences pref;
    private ListView lv;
    private ArrayAdapter<Contact> adapter;
    private ArrayList<Long> dbIDs;
    static final int PICK_CONTACT_REQUEST = 0;
    private ArrayList<Contact> contacts;
    String COUNTRY_KEY = "countryKey";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_blacklist, container, false);
        pref = getActivity().getSharedPreferences("prefs", Context.MODE_PRIVATE);
        dbIDs = new ArrayList<>();
        contacts = new ArrayList<>();
        Log.d(MainActivity.LOG_TAG, "Creating view");
        ContactsDBHelper mDbHelper = new ContactsDBHelper(getContext());
        Log.d(MainActivity.LOG_TAG, "Getting database");
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                ContactsDBContract.ContractsDBEntry._ID,
                ContactsDBContract.ContractsDBEntry.COLUMN_NAME,
                ContactsDBContract.ContractsDBEntry.COLUMN_NUMBER,
                ContactsDBContract.ContractsDBEntry.COLUMN_BLACKLIST,
                ContactsDBContract.ContractsDBEntry.COLUMN_EMERGENCY
        };

        Cursor cursor = db.query(
                ContactsDBContract.ContractsDBEntry.TABLE_NAME,
                projection, null, null, null, null, null
        );
        // convert each row in database to string
        while(cursor.moveToNext()) {
            dbIDs.add(cursor.getLong(cursor.getColumnIndexOrThrow(ContactsDBContract.ContractsDBEntry._ID)));
            String nm = cursor.getString(cursor.getColumnIndexOrThrow(ContactsDBContract.ContractsDBEntry.COLUMN_NAME));
            String num = cursor.getString(cursor.getColumnIndexOrThrow(ContactsDBContract.ContractsDBEntry.COLUMN_NUMBER));
            Log.d(MainActivity.LOG_TAG, cursor.getString(cursor.getColumnIndexOrThrow(ContactsDBContract.ContractsDBEntry.COLUMN_BLACKLIST)));
            Log.d(MainActivity.LOG_TAG, cursor.getString(cursor.getColumnIndexOrThrow(ContactsDBContract.ContractsDBEntry.COLUMN_EMERGENCY)));
            boolean b = cursor.getString(cursor.getColumnIndexOrThrow(ContactsDBContract.ContractsDBEntry.COLUMN_BLACKLIST)).equals("True");
            boolean e = cursor.getString(cursor.getColumnIndexOrThrow(ContactsDBContract.ContractsDBEntry.COLUMN_EMERGENCY)).equals("True");
            contacts.add(new Contact(nm, num, b, e));
        }
        cursor.close();
        adapter = new ContactListAdapter(contacts, getContext());
        lv = (ListView) view.findViewById(R.id.listView_blacklist);
        lv.setAdapter(adapter);
        Button manual = (Button) view.findViewById(R.id.bt_contact_manual);
        manual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = getActivity().getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.manual_contact_dialog, null);
                dialogBuilder.setView(dialogView);

                final EditText nameView = (EditText) dialogView.findViewById(R.id.enter_contact_name);
                final EditText numberView = (EditText) dialogView.findViewById(R.id.enter_contact_numb);
                final CheckBox black = (CheckBox) dialogView.findViewById(R.id.check_b);
                final CheckBox emerg = (CheckBox) dialogView.findViewById(R.id.check_e);

                dialogBuilder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ContactsDBHelper mDbHelper = new ContactsDBHelper(getContext());
                        SQLiteDatabase db = mDbHelper.getWritableDatabase();
                        ContentValues values = new ContentValues();
                        values.put(ContactsDBContract.ContractsDBEntry.COLUMN_NAME, nameView.getText().toString());
                        values.put(ContactsDBContract.ContractsDBEntry.COLUMN_NUMBER, convertNum(numberView.getText().toString()));
                        if (black.isChecked()) {
                            values.put(ContactsDBContract.ContractsDBEntry.COLUMN_BLACKLIST, "True");
                        }
                        else {
                            values.put(ContactsDBContract.ContractsDBEntry.COLUMN_BLACKLIST, "False");
                        }
                        if (emerg.isChecked()) {
                            values.put(ContactsDBContract.ContractsDBEntry.COLUMN_EMERGENCY, "True");
                        }
                        else {
                            values.put(ContactsDBContract.ContractsDBEntry.COLUMN_EMERGENCY, "False");
                        }
                        long newID = db.insert(ContactsDBContract.ContractsDBEntry.TABLE_NAME, null, values);
                        contacts.add(new Contact(nameView.getText().toString(),
                                        convertNum(numberView.getText().toString()),
                                        black.isChecked(), emerg.isChecked()));
                        dbIDs.add(newID);
                        lv.invalidateViews();
                        adapter.notifyDataSetChanged();
                    }
                });

                dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        return;
                    }
                });

                dialogBuilder.setTitle("Add Contact");
                AlertDialog dialog = dialogBuilder.create();
                dialog.show();
            }
        });
        registerForContextMenu(lv);
        return view;
    }

    /**
     * Creates a context menu (edit, delete).
     * @param menu the context menu
     * @param v the view
     * @param menuInfo the menu information
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId()== R.id.listView_blacklist) {
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.contacts_list_edit, menu);
        }
    }

    /**
     * Handles behavior when a menu item is selected.
     * @param item the menu item
     * @return true if the selection was valid, false if not
     */
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final int position = info.position;
        switch(item.getItemId()) {
            case R.id.action_edit:
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = getActivity().getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.manual_contact_dialog, null);
                dialogBuilder.setView(dialogView);

                final EditText nameView = (EditText) dialogView.findViewById(R.id.enter_contact_name);
                final EditText numberView = (EditText) dialogView.findViewById(R.id.enter_contact_numb);
                final CheckBox black = (CheckBox) dialogView.findViewById(R.id.check_b);
                final CheckBox emerg = (CheckBox) dialogView.findViewById(R.id.check_e);
                Contact toEdit = contacts.get(position);
                nameView.setText(toEdit.getName());
                numberView.setText(toEdit.getNumber());
                black.setChecked(toEdit.getBlacklist());
                emerg.setChecked(toEdit.getEmergency());
                dialogBuilder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ContactsDBHelper mDbHelper = new ContactsDBHelper(getContext());
                        SQLiteDatabase db = mDbHelper.getWritableDatabase();
                        ContentValues values = new ContentValues();
                        values.put(ContactsDBContract.ContractsDBEntry.COLUMN_NAME, nameView.getText().toString());
                        values.put(ContactsDBContract.ContractsDBEntry.COLUMN_NUMBER, convertNum(numberView.getText().toString()));
                        if (black.isChecked()) {
                            values.put(ContactsDBContract.ContractsDBEntry.COLUMN_BLACKLIST, "True");
                        }
                        else {
                            values.put(ContactsDBContract.ContractsDBEntry.COLUMN_BLACKLIST, "False");
                        }
                        if (emerg.isChecked()) {
                            values.put(ContactsDBContract.ContractsDBEntry.COLUMN_EMERGENCY, "True");
                        }
                        else {
                            values.put(ContactsDBContract.ContractsDBEntry.COLUMN_EMERGENCY, "False");
                        }
                        long newID = db.update(ContactsDBContract.ContractsDBEntry.TABLE_NAME,
                                values, " _id=" + dbIDs.get(position), null);
                        contacts.set(position,
                                new Contact(nameView.getText().toString(),
                                convertNum(numberView.getText().toString()),
                                black.isChecked(), emerg.isChecked()));
                        dbIDs.set(position, newID);
                        lv.invalidateViews();
                        adapter.notifyDataSetChanged();
                        Toast.makeText(getContext(),
                                "Contact updated!",
                                Toast.LENGTH_SHORT).show();
                    }
                });

                dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        return;
                    }
                });
                AlertDialog dialogEdit = dialogBuilder.create();
                dialogEdit.show();
                return true;
            case R.id.action_delete: // open a confirmation dialog
                AlertDialog.Builder build = new AlertDialog.Builder(getActivity());
                build.setTitle("Delete lesson?")
                        .setMessage("Are you sure you want to delete this lesson?");
                // delete element if "yes" button is clicked
                build.setPositiveButton("Delete Lesson", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ContactsDBHelper mDbHelper = new ContactsDBHelper(getContext());
                        SQLiteDatabase db = mDbHelper.getWritableDatabase();
                        db.delete(ContactsDBContract.ContractsDBEntry.TABLE_NAME,
                                "_id="+dbIDs.get(position), null);
                        contacts.remove(position);
                        dbIDs.remove(position);
                        adapter.notifyDataSetChanged();
                        lv.invalidateViews();
                        Toast.makeText(getContext(),
                                "Contact deleted!",
                                Toast.LENGTH_SHORT).show();
                    }
                });
                // do nothing if "no" button is clicked
                build.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        return;
                    }
                });
                // show the dialog
                AlertDialog dialogDelete = build.create();
                dialogDelete.show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Contacts");
    }


    private String convertNum(String input) {
        String s = "";
        for (int i = 0; i < input.length(); i++) {
            if (Character.isDigit(input.charAt(i))) {
                s = s + input.charAt(i);
            }
        }
        if (s.length() == 10) {
            s = pref.getString(COUNTRY_KEY, "1") + s;
        }
        s = "+" + s;
        return s;
    }

}
