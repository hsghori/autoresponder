package com.example.hsgho_000.autoresponder;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Activity associated with app settings.
 */

public class SettingsActivity extends AppCompatActivity {

    final String LOG_TAG = MainActivity.LOG_TAG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }
}
