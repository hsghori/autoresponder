package com.example.hsgho_000.autoresponder;

/**
 * Created by haroon on 4/26/17.
 */

public class Contact {

    private String name;
    private String number;
    private boolean blacklist;
    private boolean emergency;

    public Contact(String nm, String num, boolean black, boolean emer) {
        this.name = nm;
        this.number = num;
        this.blacklist = black;
        this.emergency = emer;
    }

    public String getName() {
        return this.name;
    }

    public String getNumber() {
        return this.number;
    }

    public boolean getBlacklist() {
        return this.blacklist;
    }

    public boolean getEmergency() {
        return this.emergency;
    }

    public void setName(String n) {
        this.name = n;
    }

    public void setNumber(String n) {
        this.number = n;
    }
}
