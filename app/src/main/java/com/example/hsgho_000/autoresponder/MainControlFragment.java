package com.example.hsgho_000.autoresponder;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.AudioManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class MainControlFragment extends Fragment {

    protected boolean stateText;
    protected String message;
    SharedPreferences pref;
    public static final String LOG_TAG = "hg";
    String MESSAGE_KEY = "messageKey";
    String STATE_KEY = "stateKey";
    String DEFAULT_RING_KEY = "defaultRingKey";
    String ACTIVE_RING_KEY = "activeRingKey";
    View view;

    public MainControlFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_main_control, container, false);
        PreferenceManager.setDefaultValues(getActivity(), R.xml.preferences, false);
        stateText = false;
        pref = getActivity().getSharedPreferences("prefs", Context.MODE_PRIVATE);
        TextView messageView = (TextView) view.findViewById(R.id.message_display);
        Switch onSwitch = (Switch) view.findViewById(R.id.on_switch);
        Button massText = (Button) view.findViewById(R.id.mass_text);
        massText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContactsDBHelper mDbHelper = new ContactsDBHelper(MainActivity.getContext());
                ArrayList<String> emergencyList = new ArrayList<>();
                SQLiteDatabase db = mDbHelper.getReadableDatabase();

                // Define a projection that specifies which columns from the database
                // you will actually use after this query.
                String[] projection = {
                        ContactsDBContract.ContractsDBEntry.COLUMN_NUMBER,
                };
                // Filter results WHERE "title" = 'My Title'
                String selection = ContactsDBContract.ContractsDBEntry.COLUMN_EMERGENCY + " = ?";
                String[] selectionArgs = { "True" };

                Cursor cursor = db.query(
                        ContactsDBContract.ContractsDBEntry.TABLE_NAME,
                        projection, selection, selectionArgs, null, null, null
                );
                // convert each row in database to string
                while(cursor.moveToNext()) {
                    String num = cursor.getString(cursor.getColumnIndexOrThrow(ContactsDBContract.ContractsDBEntry.COLUMN_NUMBER));
                    emergencyList.add(num);
                }
                cursor.close();
                SmsManager smsManager = SmsManager.getDefault();
                for (String phoneNumber : emergencyList) {
                    smsManager.sendTextMessage(phoneNumber, null, message, null, null);
                    Log.d(TAG, "Sending text to " + phoneNumber);
                }
            }
        });
        messageView.setText(pref.getString(MESSAGE_KEY, ""));
        message = messageView.getText().toString();
        onSwitch.setChecked(pref.getBoolean(STATE_KEY, false));
        onSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editState(v);
            }
        });
        return view;
    }

    public void editState(View v) {
        Switch onSwitch = (Switch) v;
        stateText = onSwitch.isChecked();
        SharedPreferences.Editor prefEdit = pref.edit();
        AudioManager ringer = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        NotificationManager mNotificationManager
                = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);;
        int mId = 1;
        if (stateText) {
            prefEdit.putBoolean(STATE_KEY, true);
            switch (pref.getString(ACTIVE_RING_KEY, "")) {
                case "Loud":
                    ringer.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                    break;
                case "Vibrate":
                    ringer.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
                    break;
                case "Silent":
                    ringer.setRingerMode(AudioManager.RINGER_MODE_SILENT);;
                    break;
                default:
                    ringer.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                    break;
            }
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(getActivity())
                            .setSmallIcon(R.drawable.icon)
                            .setContentTitle("Auto Responder")
                            .setContentText("Active");

            // Creates an explicit intent for an Activity in your app
            Intent resultIntent = new Intent(getActivity(), MainActivity.class);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(getActivity());
            // Adds the back stack for the Intent (but not the Intent itself)
            stackBuilder.addParentStack(MainActivity.class);
            // Adds the Intent that starts the Activity to the top of the stack
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent (
                            0,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
            mBuilder.setContentIntent(resultPendingIntent);
            mBuilder.setOngoing(true);
            mNotificationManager =
                    (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
            // mId allows you to update the notification later on.
            mNotificationManager.notify(mId, mBuilder.build());
        }
        else {
            prefEdit.putBoolean(STATE_KEY, false);
            switch (pref.getString(DEFAULT_RING_KEY, "")) {
                case "Loud":
                    ringer.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                    break;
                case "Vibrate":
                    ringer.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
                    break;
                case "Silent":
                    ringer.setRingerMode(AudioManager.RINGER_MODE_SILENT);;
                    break;
                default:
                    ringer.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                    break;
            }
            mNotificationManager.cancel(1);
        }
        prefEdit.apply();
    }
}
