package com.example.hsgho_000.autoresponder;

/**
 * Created by haroon on 4/26/17.
 */

import android.provider.BaseColumns;

/**
 * Contract class for contacts database. Defines LessonLogFragment.db.
 */
public class ContactsDBContract {

    private ContactsDBContract() {}

    /* Inner class that defines the table contents */
    public static class ContractsDBEntry implements BaseColumns {
        public static final String TABLE_NAME = "contacts_table";// column 0
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_NUMBER = "number";
        public static final String COLUMN_BLACKLIST = "blacklist";
        public static final String COLUMN_EMERGENCY = "emergency";

        public static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE IF NOT EXISTS " + ContractsDBEntry.TABLE_NAME + " (" +
                        ContractsDBEntry._ID + " INTEGER PRIMARY KEY, " +
                        ContractsDBEntry.COLUMN_NAME + " TEXT, " +
                        ContractsDBEntry.COLUMN_NUMBER + " TEXT, " +
                        ContractsDBEntry.COLUMN_BLACKLIST + " TEXT, " +
                        ContractsDBEntry.COLUMN_EMERGENCY + " TEXT);";

        public static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + ContractsDBEntry.TABLE_NAME;
    }
}

